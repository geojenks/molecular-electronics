% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
\documentclass[
 reprint,amsmath,amssymb,aps]{revtex4-1}

\usepackage[english]{babel}
\usepackage{scrextend}
\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
%\usepackage{bm}% bold math

\begin{document}

\preprint{APS/123-QED}

\title{Molecular Rectification by Means of Environmental Asymmetry}% Force line breaks with \\

\author{George Jenkinson}
 \altaffiliation[george.jenkinson@student.auc.nl]{Amsterdam University College}

\makeatletter
\setlength{\@fptop}{0pt}
\makeatother

\date{\today}

\begin{abstract}
Through outlining some of the purposes and concepts behind molecular electronics, and an investigation into a particular experiment \cite{diode} which demonstrates a novel way to create a molecular rectifier, a specific area of nanoscience is investigated and set in its place in the wider field.
\end{abstract}


\maketitle

%\tableofcontents

\section{\label{sec:level1}Lit Review}
\subsection{\label{sec:level2}Introduction}
%Wires (Carbon Nanotubes), transistors, and rectifiers needed for set of molecular devices. 

Electronic components have been miniaturising at an exponential rate since their realisation, following a rule of thumb, outlined in 1965 by Intel’s cofounder Gordon Moore, from whom the law takes its name, that transistor density on integrated circuits would double every year\cite{Moore}. As transistor size reduces into the mesoscopic scale, the manufacturers concerned with continuing the trend of such components are faced with an unprecedented challenge; the quantum properties of electrons bear significance at scales of ~7nm and smaller\cite{nojunct}\cite{singmol}\cite{NSgraphene}. Although there have been many theoretical and practical demonstrations showing that there remain ways to continue miniaturising components, either by continuing to miniaturise electrical components in their existing form \cite{5nm} or by using new methods or materials \cite{nojunct}\cite{grapheneqd}\cite{sub-10nm}, due to their great expense, incompatibility, and often infeasible environmental requirements, it remains unclear how, or if, smaller components will be made whilst remaining practical and marketable.

In the most commonly used transistor, the metal–oxide–semiconductor field-effect transistor (MOSFET), the oxide (or other dielectric) layer acts as an insulator to the electrical gate, which reduces subthreshold drain current - the power undesirably consumed when the transistor is in its 'off' state. Using tradition silicon oxides, as the layer is reduced to lengths below ~7nm, the effect of quantum tunneling across the insulating layer becomes significant enough to detrimentally affect the functionality of the transistor\cite{MOSFET-tunnel}. To avoid this problem, manufacturers have had to investigate other dielectric materials, but often shrinking the components comes at the price of reduced efficiency, or significantly reduced capability in respect to transmitting power\cite{MOSFET-tunnel}.

Molecular electronics seeks to find molecular analogues of electric components, in such a way that connected electronic circuits can be built starting from individual molecules. Working with transfers that utilise single electrons is truly the lower limit of classical electronics in respect to power efficiency. Molecular electronics is interesting not only because it has technological implications, but also because it reveals truths about the natural world, and appears to be closely linked to DNA.

\subsection{\label{sec:level2}Marcus Theory}

For the field of molecular electronics, the control of electron transfers in and between molecules is crucial. For this reason a good understanding of these processes is required\cite{Marcus}; especially the transfer rate is of great interest.

A model that helps with understanding electron transfer, as well as explaining the rate of it, is the Marcus Theory. It was developed by Rudolph A. Marcus in 1956, who received a nobel prize for his work, to explain outer sphere electron transfer from an electron donor to an acceptor. It was later extended to include inner sphere transfers, which differ from outer sphere transfers, not only in the charge of the chemical species involved, but also in structure\cite{e-transfer}.

Marcus theory is the simplification of a more general, but also more complicated model, the Eyring´s transition state theory. It holds for electron transfer reactions of the following form, in which no new bonds are created and none are broken\cite{RRKM}.
\begin{equation*}
    A(red)+B(ox)\rightarrow A(ox)+B(red)
\end{equation*}
Marcus developed his theory as an addition to the Frank-Condon principle applied to electron transfer. The Frank-Cordon principle explains the slow rate of electron transfer reactions in solutions, by an energy barrier. It says that in an electron transfer reaction in solution, a reaction energy barrier has to be considered resulting from the fact that the electrons jump considerably faster than the atomic nuclei are able to move. This causes the products to form in a charge environment that is favourable to the reactant. This distribution of charges can be seen as an energetic barrier for the electron transfer to occur\cite{Marcus}.

However, Marcus found that this model fails to explain electron transfer reactions taking place in the dark. The following reaction illustrates this problem.
% http://www2.mpip-mainz.mpg.de/~andrienk/journal_club/marcus.pdf
\begin{equation*}
    \left[ Fe(H_{2}O)_{6} \right] ^{2+} \left[ Fe(H_{2}O)_{6} \right] ^{3+} \rightarrow \left[ Fe(H_{2}O)_{6}\right] ^{3+} \left[ Fe(H_{2}O)_{6}\right] ^{4+}
\end{equation*}
The equilibrium distance between Fe and O is about 2.21Å in the Fe(II) ion, while it is about 2.05Å for Fe(III)\cite{Marcus}. Since the electron transfer is too fast for the other atoms to move as well, an electron transfer from Fe(II) to Fe(III) would result in a compression of the first and a stretching of the latter. These would release energy, since they are in excited vibrational states, which could not conserve energy if happening in the dark, i.e. photons were not being added to the system. For this reason Marcus concluded that the electron can only be transferred when the free energy of the reactants immediately before the reaction and the free energy of the products are the same\cite{RRKM}.

Marcus theory treats the motion required to reach the transition state as classical thermal motion, which is a good approximation for high temperatures and heavy reactants. The most important point of Marcus theory is that it gives an accurate method to calculate the activation energy $\triangle G^{*}$, as well as the reaction rate $k_{rate}$ of electron transfer reactions in terms of the reorganization energy $\lambda$ and the standard free energy $\triangle G^{\begin{tiny}
o
\end{tiny}}$. The following formulas describe this dependence \cite{RRKM}:
\begin{equation*}
\triangle G^{*} = (\dfrac{\lambda}{4})\cdot (1 + \dfrac{G^{\begin{tiny} o \end{tiny}}}{\lambda})^{2}
\end{equation*}
\begin{equation*}
k_{rate} = A\cdot e^{\frac{-\triangle G^{*}}{{k_{b}T}}}
\end{equation*}
Marcus theory holds for non-adiabatic reactions in which the coupling of the electron donor and acceptor is weak\cite{Marcus}, and provides a foundation on which to base electron transfer between molecules; molecular electronics.

\subsection{\label{sec:level2}Huckel Method}

A solid grasp of molecular electron behaviour is necessary to fully understand what concepts make molecular electronics possible. It describes the pi systems of simple molecules, allowing us to understand the nature of the molecular orbitals.  Many such methods exist, some of which are computationally complex, such as the Hartree-Fock method or Density Functional Theory, and some of which can be solved analytically such as band theory and the Huckel method \cite{book}. 

Proposed by Erich Huckel in 1930, the Huckel method is a very simple way to determine the Molecular Orbital Energy as a Linear Combination of Atomic Orbital (LCAO).  Molecular Orbitals(MO’s) are the mathematical description of the electron wave function there where it has significant amplitude. The Huckel method focuses on describing MO’s for simple, highly symmetric systems, specifically those containing pi elections (indirect overlap). It arose from an attempt to describe the interactions between pi electrons, which, contrary to sigma electrons, are highly delocalised across the molecule. Sigma electrons are not described because of the way the problem is framed. 

The simplest system that is described by the Huckel method is ethene, containing only 2 pi electrons and 2 carbon atoms. 

The MO is described by a LCAO:
\begin{equation*}
    \Psi = a\varphi + b\phi
\end{equation*}
Where a and b are constants and $\varphi$ and $\phi$ are atomic orbitals. 

Substituting this into the Schrodinger equation yields:
\begin{equation*}
    Ha\varphi + Hb\phi = Ea\varphi + Eb\phi
\end{equation*}
Now we left multiply by $\varphi$ and $\phi$ and integrate to find two different equations. This is where a bunch of approximations come in. First we assume that $\varphi$ and $\phi$ are orthonormal, making their overlap integral 0 or 1 depending on whether we integrate across the same or different atomic orbitals. 

Secondly, we invoke the symmetry of the molecule and assume that $\varphi H \varphi = \phi H\phi$ and we call this $\gamma$. 
We do the same for Hamiltonians acting on two different atomic orbitals. $\varphi H \phi = \phi H\varphi$ and we call this $\varepsilon$.

This yields the following equations:
\begin{equation*}
a(\gamma - E) + b(\varepsilon ) = 0
\end{equation*}
\begin{equation*}
a(\varepsilon ) + b(\gamma - E) = 0
\end{equation*}
We solving this system of linear equations we find the possible MO energies to be:
\begin{equation*}
E = \gamma \mp \varepsilon
\end{equation*}
 Substituting this back and invoking normalisation on ψ:
\begin{equation*}
\Psi = \dfrac{1}{2}(\varphi \mp \phi)
\end{equation*}
This formulation of the wave function shows us that we have a linear combination of the two atomic wave functions with each contributing equally. The lower energy wave function will be an in-phase combination and one of them being out of phase. This means that the spin orbitals will either be adjacent or diagonally across from each other when describing the atomic p-orbitals as a dumbbell extending above and below the carbon atoms. 

From this the simplicity of the Huckel method should be evident as much as its limitations. Perturbation theory it can be applied to a slightly wider variety of problems but it is far from universal. Its power lies in its simplicity in providing us with an intuitive understanding of how we can describe pi systems through the linear combination of atomic orbitals and relate the phase geometry to the energies associated to the molecular orbitals. Combining this with the Aufbau principle allows us to formulate a somewhat complete ground state description of molecular electron behaviour. We know the sigma system is irrelevant to the systems chemical reactivity due to the electrons lying in relatively deep potential wells. We can consequently ‘’fill’’ the pi-system described by the Huckel method with the non-sigma valence electrons to find a geometric and spin related description of the highest lying molecular orbital. This is extremely relevant for formulating any hypotheses on molecular electronics as molecules with desirable properties often do so because of the interactions in their pi-systems. By understanding the way these pi-systems are interrelated and, more importantly, how we can modify them, we can come up with structures that have component like properties. 


In this we assume that the integrals involving the Hamiltionian of $\phi$ and $\phi$ are equal to the Hamiltonian $\Phi$ and $\Phi$ by invoking the symmetry of the molecule.
We do the same for Hamiltonians acting on two different atomic orbitals. We call these $\gamma$ and $\epsilon$ respectively. 

Assuming the overlap of non-equal atomic orbitals to be zero we get:
\begin{equation*}
    a(\gamma - E)+b(\epsilon) = 0
\end{equation*}
and:
\begin{equation*}
    a(\epsilon) + b(\gamma - E) = 0
\end{equation*}
We solving this system of linear equations we find the possible MO energies to be:
\begin{equation*}
    E = E = \gamma \pm \epsilon
\end{equation*}
 and:
\begin{equation*}
    \Psi = \frac{1}{\sqrt{2}}(\psi \pm \Psi)
\end{equation*}
From this the simplicity of the Huckel method should be evident as much as its limitations. Using perturbation theory it can be applied to a slightly wider variety of problems but it is far from universal. Its power lies in its simplicity. 

\section{\label{sec:level1}Making a Rectifier}

A rectifier is an electronic component that allows current to flow in one direction, whilst effectively  blocking current in the other direction. Classically, this rectifying ability is achieved through an asymmetry in respect to the molecular organisation when a voltage bias is applied across the rectifier. A rectifier under forward bias allows the flow of charge, whereas under reverse bias, the device largely prohibits the flow of charges.

A number of methods have been proposed and realised to make molecular diodes\cite{diode}\cite{singmol}\cite{reso}\cite{linker}\cite{wired}, with varying success and feasibility regarding out of lab applications. The rectifying ability in molecular diodes is generally achieved by an asymmetrically shaped molecule joining electrodes over a junction, different materials serving as the electrodes either side of the junction, or by the effective contact between the molecule and electrodes being made by differing linker molecules on either end of the molecule across the junction. One group of experimenters looked in to breaking symmetry across the junction by differing the effective exposed areas of the electrode\cite{diode}. In order to understand under what conditions this results in an effective diode, it is necessary to understand the underlying physics.

\subsection{\label{sec:level2}Electric Double Layer}

Any charged surface that is in contact with an environment that can provide neutralising charges will adsorb such charges onto its surface until the surface is neutralised\cite{book}, forming an Electric Double Layer (EDL) consisting of the surface charge, the layer of oppositely charged ions (Helmholtz Layer), and a diffuse layer where the concentration of the oppositely charged ions falls off to equilibrium\cite{stern}. The charges are dispersed in the medium according to the electrostatic attraction to the surface and thermal fluctuations, the innermost charges partially screen the electric field from the outermost ones, resulting in an exponentially decreasing electric field, with respect to the distance from the charged surface.

Treating the ions as point charges, the distribution can be calculated by a Poisson-Boltzmann equation\cite{book}. The length over which the electric field reduces by Euler's constant is the Debye length. The length becomes shorter with a higher density of neutralising charges closer to the charged surface, and when this high density layer is taken in conjunction with the charged surface, it corresponds to a larger energy required to remove or add charges to the surface compared to an absence of EDL.

%Across conducting junction, use the length to find an area which is related to current. Take into account the ions are being neutralised, makng the debye length longer.

\subsection{\label{sec:level2}Stark shift}

The Stark Effect is observed when a charge distribution is placed in an external electric field. The electric field causes a separation of otherwise degenerate energy levels, which in turn shifts the HOMO and LUMO resonances of the molecule up or down.

The effect can be demonstrated in a simple case, a Hydrogen atom. The Hamiltonian of an electron in an electric field can be obtained by applying a perturbation, the Zeeman term (Hz), onto the Coulombic Hamiltonian,
\begin{equation*}
V(r) = - \dfrac{Zq^{2}}{4\pi\epsilon_{0}r}
\end{equation*}
where V is the potential as a function of distance (r) from the source, Z is the number of particles, ane q the charge of the particle. Changing to Gaussian units for convenience, for a hydrogen atom this becomes 
\begin{equation*}
V(r) = - \dfrac{e^{2}}{r}
\end{equation*}
where e is the elementary charge of an electron. On the basis that the energy of the hydrogen atom is the sum of the kinetic (${\dfrac{mv^{2}}{2} }$)and potential energies, we obtain the Hamiltonian for the electron 
\begin{equation*}
H_{Coulomb} = - \dfrac{h^{2}}{2\mu}\nabla^{2}- \dfrac{e^{2}}{r}
\end{equation*}
The Stark perturbation is due to an external Electric field ($\varepsilon$), and the electrical dipole moment;
\begin{equation*}
H_{Stark} = -{\varepsilon}{\mu}_{el}
\end{equation*}
in a Hydrogen atom, with an electron separated from a proton by a distance $x, {\mu}_{el} = e{x}$, so choosing $\varepsilon$ along the z direction gives
\begin{equation*}
H_{Stark} = -e\varepsilon z
\end{equation*}
Representing the molecule as made up of its constituent quantum numbers allows a calculation of its energy perturbations of first order. Where $n$ is the energy level at which the electron lies; $l$ the azimuthal number, describing the orbital angular momentum, or extrinsic spin, importantly influencing the shape of the subshell in which the electron lives; and $m$ the intrinsic spin, which describes the specific orbital within the subshell that the electron occupies. Considering
\begin{equation*}
H_{Stark} = -{\varepsilon}{\mu}_{el}
\end{equation*}
where ${\mu}_{el}$ is the magnetic dipole moment of the electron, and
\begin{equation*}
L_{z} = xp_{y} - p_{x}y = -ih(z\dfrac{\partial}{\partial y}-\dfrac{\partial}{\partial x}y)
\end{equation*}
which shows that $[L_{z}, z] = 0$, we can now consider
\begin{equation*}
\langle n, l, m\vert [L_{z}, z] \vert n', l', m'\rangle = (m - m')\langle n, l, m\vert z \vert n', l', m'\rangle = 0
\end{equation*}
so for every state where $(m - m') \neq 0$, $\vert n, l, m\rangle$ and $\vert z \vert n', l', m'\rangle$ form an orthonormal basis. Choosing $(m - m') = 0$ gives a non-zero solution, which we are interested in.

Using the parity operator ($P\psi(r)=\psi(-r)$), we can prove that in order to obtain a non-zero result we require $l \neq l'$. This follows from the theorem that the Coulomb Potential, as a symmetrical potential, can be expressed as a collection of basis states that consist of even and odd functions\cite{T2}. We can exploit the spherical symmetry to choose as our basis set the Legendre polynomials $P_{l}(cos\theta)$, which gives alternatively even and odd functions. This allows us to exploit the properties of the parity operator, namely that if $\vert n, l, m\rangle$ and $\vert n', l', m'\rangle$ have the same azimuthal number, then the product is an even function. The position operator $z$ is odd, meaning that the integration involved in evaluating
\begin{equation*}
\langle n, l, m\vert z \vert n', l', m'\rangle
\end{equation*} 
becomes 0 between -r and r. We can go further than $l \neq l'$, and assume $l = l' \pm 1$, which would be the case if the interaction between the electric field and electron was mediated by a particle of integer spin; a photon.

Because the ground state is non-degenerate, we can follow
\begin{equation*}
E^{(1)}_{n} = \langle n^{(0)}\vert V\vert n^{(0)}\rangle
\end{equation*} 
we find the first order perturbation
\begin{equation*}
E^{(1)}_{1,0,0} = -e\varepsilon \langle 1, 0, 0 \vert V\vert 1, 0, 0 \rangle
\end{equation*} 
which turns out to be 0, because in this case $l = l'$. The second order perturbation is then of the form
% it is not always 0****
\begin{equation*}
E^{(2)}_{n} = \lambda^{2}\sum_{k\neq n}\dfrac{\vert\langle k^{(0)}\vert V\vert n^{(0)}\rangle\vert^{2}}{E^{(0)}_{n} - E^{(0)}_{k}}
\end{equation*}
\begin{equation*}
E^{(2)}_{1,0,0} = e^{2}\varepsilon^{2}\sum_{n = 2}^{\inf}\dfrac{\vert\langle n, 1, 0\vert z\vert 1, 0, 0\rangle\vert^{2}}{E^{(0)}_{1} - E^{(0)}_{n}}
\end{equation*}
using the Bohr Formula, with the Bohr radius 
\begin{equation*}
\alpha = \dfrac{\hslash^{2}}{m_{e}e^{2}}
\end{equation*}
\begin{equation*}
E_{n} = -\dfrac{e^{2}}{2\alpha n^{2}} 
\end{equation*}
The equation becomes
\begin{equation*}
E^{(2)}_{1,0,0} = e^{2}\varepsilon^{2}\sum_{n = 2}^{\inf}\dfrac{\vert\langle n, 1, 0\vert z\vert 1, 0, 0\rangle\vert^{2}}{\dfrac{-e^{2}}{2\alpha} - \dfrac{-e^{2}}{2\alpha n^{2}}}
\end{equation*}
\begin{equation*}
E^{(2)}_{1,0,0} = -2\alpha \varepsilon^{2}\sum_{n = 2}^{\inf}\dfrac{\vert\langle n, 1, 0\vert z\vert 1, 0, 0\rangle\vert^{2}}{-1 - \dfrac{-1}{n^{2}}}
\end{equation*}
using the orthonamilty of spherical harmonics\cite{Hydro}, it can be shown that
\begin{equation*}
\vert\langle n, 1, 0\vert z\vert 1, 0, 0\rangle\vert^{2} = \dfrac{2^{8}n^{7}(n-1)^{2n-5}}{3(n+1)^{2n+5}} \alpha^{2}\delta_{l_{0}} \delta_{m_{0}}
\end{equation*}
\begin{figure*}[t]
\centering
\includegraphics[keepaspectratio, width=15cm]{graphs.jpg}
\caption{Energy level diagram illustrating the rectification mechanism for a LUMO conducting molecular junction. Adapted from \cite{diode}
\textbf{a)} Zero-bias, molecular resonance with peak energy ε relative to the tip (T) and substrate (S) Fermi levels EF.
\textbf{b)} In polar media, when the tip is biased negatively relative to the substrate, the molecular resonance is at $\varepsilon − eV$ relative to the tip chemical potential and at ε relative to the substrate chemical potential (here, $\alpha$ is taken to be 0.5). For this system, a large area (shaded green) of the resonance falls within the bias window and the current is high.
\textbf{c)} When the tip is biased positively relative to the substrate, the molecular resonance again remains pinned to the substrate chemical potential, but is at $\varepsilon + eV$ relative to the tip chemical potential. A small area (shaded red) of the resonance falls within the bias window and the current is low.
\textbf{d)} A similar schematic illustrating level alignment in a nonpolar solvent $(\alpha = 0)$ with the tip biased negatively relative to the substrate. Here, the resonance does not shift in response to the applied bias, while both tip and substrate chemical potentials shift relative to the resonance position. The area under the resonance that falls within the bias window is independent of the bias polarity.\label{fig:graphs}}
\end{figure*}
Evaluating the whole sum yields \cite{T2}
\begin{equation*}
E^{(2)}_{1,0,0} = -2.25\alpha^{3}\varepsilon^{2}
\end{equation*}
giving an electric dipole moment
\begin{equation*}
d = -\dfrac{\partial E}{\partial \varepsilon} = 4.5\alpha^{3}\varepsilon^{2}
\end{equation*}
This shows that an electric dipole is induced by the electric field, and that the previously degenerate energy state fo the electron in a Hydrogen molecule is split into non degenerate states, this is the Stark effect. The energy shift corresponds to a shift in the resonant energy of the hydrogen molecule. A similar mechanism results in a resonance shift in more complex molecules.
\subsection{Rectification}

The EDL formed when a voltage is applied across an STM-BJ \cite{STM-bj} in a polar environment is more dense on the STM-tip than the substrate due to the smaller exposed area, but equal in absolute potential. This results in a denser EDL, and hence much shorter Debye length, on the tip, meaning that its electric field has a muted effect on the molecule compared to that of the substrate, which is relatively unscreened.

This disparity in electric field results in the stark shift on the molecule as a result of the STM tip being relatively insignificant compared to that of the substrate. This means that the resonance of the electrons in the molecule are more significantly shifted by the potential of the substrate, and the resonance energy is effectively pinned to the potential of the substrate.

The current through the molecule can be modelled using a Landauer expression \cite{diode}, which relates the resistance of a conductor to its scattering, which are in turn related to the current through the resistor and its resonance.

Beginning from the assumption that the electrons between two electrodes of opposite electrochemical potentials do not scatter, that is to assume that electron interaction in the junction is ballistic; that the electrons bridging the gap are in equilibrium with the electrodes; zero temperature; and reflectionless electrode contacts, it can be shown \cite{Landauer} that the  current across the region can be expressed as
\begin{equation*}
I = \dfrac{2e}{h}\int_{lower energy}^{upper energy} T(E,\varepsilon, \Gamma )dE
\end{equation*}
Where $T(E,\varepsilon, \Gamma )$ is the energy (E) dependent transmission function for the junction.

The interpretation models that the tunneling, or electric transmission, through the molecule follows a Lorentzian curve \cite{diode}, centred at the molecule's electron orbital resonance closest to the Fermi energies of the substrate and tip. 
\begin{equation*}
T(E,\varepsilon, \Gamma ) = \dfrac{(\Gamma ^{2}/4)}{(E - \varepsilon )^{2} + (\Gamma ^{2}/4)}
\end{equation*}
This means that the peak of the Lorentzian is therefore the resonant energy at which the component most readilly transmits current. In the presense of an electric field, this resonant energy will shift, meaning that we can replace $\varepsilon$ with $\varepsilon + \alpha eV$, where $\alpha$ represents the effect of stark shift, and allows for the effective current to be graphed as the area under the Lorentzian curve that falls between the upper $(+eV)$ and lower $(-eV)$ limits dictated by the potentials of the electrodes.

The potential of the substrate will therefore be relatively fixed compared to the resonant energy of the nearest energy level electron, and the potential of the tip can be controlled in such a way that the current (Area under the curve on fig.1) through the junction can be controlled. The asymmetry of the Lorentzian about the junction Fermi energy means that an inverse of the bias results in different currents (Fig.\ref{fig:graphs}).

It can be seen in Fig.\ref{fig:data} that in a non-polar material, where no EDLs form, the rectification effect is not observed, this is because the stark shift is no more significant for the tip than it is for the substrate, meaning that the electron resonance energy does not move relative to the Fermi level of the junction. In this case a reverse bias simply carries current equal in magnitude but opposite in direction.
\begin{figure*}[t]
\centering
\includegraphics[keepaspectratio, width=15cm]{data.png}
\caption{High rectification ratios in TDOn junctions. From \cite{diode}
\textbf{a)} Average I–Vs for TDOn (with n = 3–5) measured in PC. Inset: Rectification ratio (Ion/Ioff) versus magnitude of applied voltage for I–V curves, showing ratios of ∼4 at 0.6 V for TDO3, ∼90 at 0.42 V for TDO4 and ∼200 at 0.37 V for TDO5. Also note that the junctions rupture when the bias is increased beyond a molecule-dependent critical voltage (−0.65 V for TDO3, −0.42 V for TDO4 and −0.39 V for TDO5).
\textbf{b)} Average I–Vs for TDO3–5 measured in TCB. Note that the I–V curves show the absolute value of current, for clarity. Junctions formed in TCB are typically able to withstand twice the voltage of those in PC, becoming unstable and rupturing beyond 1.1 V..\label{fig:data}}
\end{figure*}

The theory suggests that the rectification ratio ought to be higher for molecules with a resonant energy close to the Fermi level of the junction. This energy can sometimes be approximated using the Huckel method, although emperical data would prove more useful in searching for molecules with a resonance appropriate for a certain junction. The  resonance nearest to the junction's Fermi energy will also dictate the direction of rectification for the diode, as this would move the peak of the Lorentzian curve up and down in Fig. \ref{fig:graphs}. The theory also suggests that molecules which do not have strongly defined resonant peaks would not demonstrate such strong rectification, as varying the bias through the region permitted by the tolerance of the molecule would yield less of an extreme change, due to a flattening of the Lorentzian curve. As the theory is based around EDLs, the effect should only be observable in polar environments in which the EDL is allowed to form. These predictions are backed up by experimental data (Fig. \ref{fig:data}).

\section{Discussion}

The method demonstrates a generally applicable mechanism through which a high level of rectification can be achieved at  a molecular level. The extant difficulties arising from other factors are not addressed in the outlined method, however the asymmetrically strutted junction should be portable to other junctions beyond STM-BJ, and the mechanism should remain be unaffected by higher temperatures\cite{diode}.

An inherent drawback with the method is that it relies on significantly differing exposed electrode areas which can accomodate EDLs, this ultimately would be unacheivable on the atomic scale regarding a device made completely of molecular components. However the method grants insight into ionic liquid gating in all contexts, and reveals something about the nature of energy flow at the nanoscale.

Molecular electronics remains an exciting and bountiful area of research, however it has not yet had the breakthrough that parallels silicon based electronics, in the respect that research powers the technological and financial means to do further research. Steps forward in the effectiveness of molecular components do however benefit from a more multidisciplinary nature, in that they much more closely parallel phenomena of the biological world\cite{DNA}. Although a complete set of mesescopic devices remains theoretical, the emerging technologies appear to be converging towards a future of molecular devices\cite{diode}\cite{NSgraphene}\cite{sub-10nm}\cite{wired}.

% Coulomb Blocking
% (Finfet transistors)
\clearpage
\begin{thebibliography}{9}
\section*{Bibliography}

\bibitem{diode}
Capozzi, B.; Xia, J; Adak, O; Dell, E.J.; Liu, Z.F.; Taylor, J.C.; Neaton J.B.; Campos, L.M.; Venkataraman, L. (2015)
\textit{Single-molecule diodes with high rectification ratios through environmental control}
Nature Nanotechnology 10 522-527

\bibitem{Moore}
Moore, G.E. (Apr. 19, 1965)
\textit{Cramming More Components onto Integrated Circuits}
Electronics, 114–117, April 19, 1965

\bibitem{nojunct}
Colinge, J.P.; Lee, C.W.; Afzalian, A.; Dehdashti, N.; Yan, R.; Ferain, I.; Razavi, P.; O’Neill, B.; Blake, A.; White, M.; Kelleher, A.M.; McCarthy, B.; Murphy, R. (Feb. 21, 2010)
\textit{Nanowire transistors without junctions}
Nature Nanotechnology 5: 225-229 [doi: 10.1038/nnano.2010.15]

\bibitem{singmol}
Sriharsha V. A.; Venkataraman, L (Jun. 5, 2013)
\textit{Single-molecule junctions beyond electronic transport}
Nature Nanotechnology 8: 399-410. [doi: 10.1038/nnano.2013.91]

\bibitem{NSgraphene}
Inman, M. (Apr. 17, 2008)
\textit{Atom-thick material runs rings around silicon}
New Scientist, 198 (2653).

\bibitem{5nm}
Lee, H.; Yu, L.; Ryu, S. W.; Han, J.W.; Jeon, K.; Jang, D.Y.; Kim, K.H.; Lee, K.; Kim, J.H.; Jeon, S.C.; Lee, G.S.; Oh, J. S.; Park, Y.C.; Bae, W.H.; Lee, H. M.; Yang*, J. M.; Yoo, J. J.; Kim S.I.; Choi Y.K.; (Jun. 13, 2006)
\textit{Sub-5nm All-Around Gate FinFET for Ultimate Scaling}
2006 Symposium on VLSI Technology, 2006. Digest of Technical Papers. 58-59 [doi: 10.1109/vlsit.2006.1705215]

\bibitem{grapheneqd}
Ponomarenko, L. A.; Schedin, F.; Katsnelson, M. I.; Yang, R.; Hill E. W.; Novoselov, K. S.; Geim A. K. (Apr. 18 2008)
\textit{Chaotic Dirac Billiard in Graphene Quantum Dots}
Science 320 (5874) 356-358. [doi: 10.1126/science.1154663]

\bibitem{sub-10nm}
Zheng, J.; Wang, L.; Quhe, R.; Liu, Q.; Li, H.; Yu, D.; Mei, W.N.; Shi, J.; Gao, Z.; Lu, J. (Feb. 19, 2013)
\textit{Sub-10 nm Gate Length Graphene Transistors: Operating at Terahertz Frequencies with Current Saturation}
Nature Scientific Reports 3 (1314):1-9 [doi: 10.1038/srep01314]

\bibitem{MOSFET-tunnel}
Jaud, M.-A.; Barraud S.; Le Carval, G. (Mar. 7, 2004)
\textit{Impact of quantum mechanical tunneling on off-leakage current in double-gate MOSFET using a quantum drift-diffusion model}
2004 NSTI Nanotechnology Conference and Trade Show 2:17-20 [isbn:  0-9728422-8-4]

\bibitem{Marcus}
Andrea, M. (2008)
\textit{Marcus theory for electron transfer a short introduction}
MPIP J. Club-Mainz:1–13.

\bibitem{e-transfer}
Lesse, H.S. (2011)
\textit{Theory of electron transfer Electron transfer. Part II}
Winterschool Theor. Chem. Spectrosc.:12–16.

\bibitem{RRKM}
Di Giacomo, F. (2015)
\textit{A short account of RRKM theory of unimolecular reactions and of marcus theory of electron transfer in a historical perspective}
J. Chem. Educ. 92:476–481.

\bibitem{book}
Lindsay, S.M. (2011)
\textit{Introduction to Nanoscience.} Oxford: Oxford University Press.

\bibitem{reso}
Perrin, M.L.; Galan, E.; Eelkema, R.; Grozema, F.; Thijssen, J.M.; van der Zant, H.S.J.;(2015)
\textit{Single-Molecule Resonant Tunneling Diode}
J. Phys. Chem. C 2015, 119, 5697−5702 [doi: 10.1021/jp512803s]

\bibitem{linker}
Batra, A. et al. (2013)
\textit{Tuning Rectification in Single-Molecular Diodes}
Nano Lett. 13, 6233–6237

\bibitem{wired}
Service R.F. (Dec. 21, 2001)
\textit{Molecules Get Wired}
Science 294 (5551), 2442-2443. [doi: 10.1126/science.294.5551.2442]

\bibitem{stern}
Schneider C. (2010)
\textit{The Surface Charge of Soft and Hard Sphere Colloidal Particles – Experimental Investigation and Comparison to Theory (Dissertation)}
Helmoltz Zentrum, Berlin HZB – B 16

\bibitem{T2}
Bertlmann R.A. (2008)
\textit{Theoretical Physics T2 Quantum Mechanics - Course Lectures}
University of Vienna, T2–Script of Sommersemester

\bibitem{Hydro}
Branson J. (Apr. 2013)
\textit{Hydrogen Atom Ground State in a E-field, the Stark Effect}
San Diego University College, URL: $http://quantummechanics.ucsd.edu/ph130a/130_notes/node337.html$

\bibitem{STM-bj}
Xu, B. Q.; Tao, N. J. (2003)
\textit{Measurement of single-molecule resistance by repeated formation of molecular junctions}
Science 301, 1221–1223

\bibitem{Landauer}
Galperin Y.M. (1998)
\textit{Quantum Transport Lecture Notes}
Lund University, URL: $http://folk.uio.no/yurig/quTpdf.pdf$

\bibitem{DNA}
Genereux, J. C.; Barton, J. K. (May, 2009)
\textit{DNA charges ahead}
Nature Chemistry 1:106-107

\end{thebibliography}
\end{document}
