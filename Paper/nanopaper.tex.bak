% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
\documentclass[
 reprint,amsmath,amssymb,aps]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math

\begin{document}

\preprint{APS/123-QED}

\title{Molecular Electronics}% Force line breaks with \\

\author{George Jenkinson}
 \altaffiliation[george.jenkinson@student.auc.nl]{Amsterdam University College}
\author{Onno}%
 \email{Onno's email}
\affiliation{AUC}
\author{Bram}
 \email{Bram's email}
\affiliation{AUC}

\date{\today}

\begin{abstract}
This paper is about molecular electronics\cite{wired}
\end{abstract}


\maketitle

%\tableofcontents

\section{\label{sec:level1}Lit Review}
%Current state, Moore's law. Shit's getting real small, so small that electrons can no longer be thought of as continuous charge, but have to be considered according to quantum laws (each electron is significant, Coulomb Blockade). 

%Wires (Carbon Nanotubes), transistors, and rectifiers needed for set of molecular devices. 

Electronic components have been miniaturising at an exponential rate since their realisation. As the size reduces into the mesoscopic scale, the manufacturers concerned with continuing the trend of such components are faced with an unprecedented challenge; the quantum properties of electrons bears significance at scales of ~7nm and smaller\cite{singmol}\cite{NSgraphene}\cite{nojunct}. Although there have been many theoretical and practical demonstrations showing that there remain ways to continue miniaturising components, either by continuing to miniaturise electrical components in their existing form \cite{5nm} or by using new methods or materials \cite{grapheneqd}\cite{nojunct}\cite{sub-10nm}, due to their great expense, incompatibility, and often infeasible environmental requirements, it remains unclear how, or if, smaller components will be made whilst remaining practical and marketable.
% Describe MOSFET, why that structure places limits on particularly transistors. As molecular electronics does away with this, it is a viable solution, hence loads of research.}

In the most commonly used transistor, the metal–oxide–semiconductor field-effect transistor (MOSFET), the oxide (or other dielectric) layer acts as an insulator to reduce subthreshold drain current, which undesirably consumes power when the transistor is in its 'off' state. Using tradition silicon oxides, as the layer is reduced to lengths below ~7nm, the effect of quantum tunneling across the insulating layer becomes significant enough to detrimentally affect the functionality of the transistor\cite{MOSFET-tunnel}. To avoid this problem, manufacturers have had to investigate other dielectric materials, but often shrinking the components comes at the price of reduced work efficiency, or significantly reduced power threshold\cite{MOSFET-tunnel}.

Molecular electronics seeks to find molecular analogues of electric components, in such a way that connected electronic circuits can be built starting from individual molecules. Working with transfers that utilise single electrons is truly the lower limit of classical electronics in respect to power efficiency. Molecular electronics is interesting not only because it has technological implications, but also because it reveals truths about the natural world, and appears to be closely linked to DNA.

% Cut down unnecessary talk around the subject in intro.

\subsection{\label{sec:level2}Marcus Theory}

For the field of molecular electronics the control of electron transfers in and between molecules is crucial. For this reason a good understanding of these processes is required\cite{Marcus}; especially the transfer rate is of great interest.
A model that helps with understanding electron transfer, as well as explaining the rate of it, is the Marcus Theory. It was developed by Rudolph A. Marcus in 1956, who received a noble price for it, to explain outer sphere electron transfer from an electron donor to an acceptor. Later it was extended to include inner sphere transfers, which differ from outer sphere transfers, because not only the charge of the involved chemical species, but also their structure changes significantly \cite{e-transfer}.
Marcus theory is the simplification of a more general, but also more complicated model, the Eyring´s transition state theory. It holds for electron transfer reactions, reactions of the following form, in which no new bonds are created and none are broken \cite{RRKM}.

\begin{equation*}
    A(red)+B(ox)→A(ox)+B(red)
\end{equation*}

Marcus developed his theory as an addition to the Frank-Condon principle applied to electron transfer. In an electron transfer reaction in solution, a reaction energy barrier has to be considered resulting from the fact that the electrons jump considerably faster than the atomic nuclei are able to move. This causes the products to form in a charge environment that is favourable to the reactant. This distribution of charges can be seen as an energetic barrier for the electron transfer to occur \cite{RRKM}. Marcus theory holds for non-adiabatic reactions in which the coupling of the electron donor and acceptor is weak. It allows us to calculate the activation energy of electron transfer reactions from the Gibbs free energy and a factor called the reorganisation energy. On top of that it represents a way to calculate the pre factor in the Arrhenius equation, and therefore the reaction rate, from the strength of the coupling of the electron donor and acceptor.

\subsection{\label{sec:level2}Huckel Method}

A solid grasp of molecular electron behaviour is necessary to fully understand what concepts make molecular electronics possible. Many such methods exist, some of which are computationally complex, such as the Hartree-Fock method or Density Functional Theory, and some of which can be solved analytically such as band theory and the Hückel method . 
Proposed by Erich Huckel in 1930, the Huckel method is a very simple way to determine the Molecular Orbital Energy as a Linear Combination of Atomic Orbital (LCAO).  Molecular Orbitals(MO’s) are the mathematical description of the electron wave function there where it has significant amplitude. The Huckel method focuses on describing MO’s for simple, highly symmetric systems, specifically those containing pi elections (indirect overlap).
The simples system that is described by the Huckel method is ethene, containing only 2 pi electrons and 2 carbon atoms. 

The MO is described by a LCAO:

\begin{equation*}
    \Psi = a\phi + b\Phi
\end{equation*}

Where a and b are constants and $\phi$ and $\Phi$ are atomic orbitals. 

Substituting this into the Schrodinger equation yields:

\begin{equation*}
    Ha\phi + Hb\Phi = Ea\phi + Eb\Phi
\end{equation*}

Now we left multiply by $\phi$ and $\Phi$ and integrate to find two different equations. In this we assume that the integrals involving the Hamiltionian of $\phi$ and $\phi$ are equal to the Hamiltonian $\Phi$ and $\Phi$ by invoking the symmetry of the molecule.
We do the same for Hamiltonians acting on two different atomic orbitals. We call these $\gamma$ and $\epsilon$ respectively. 

Assuming the overlap of non-equal atomic orbitals to be zero we get:

\begin{equation*}
    a(\gamma - E)+b(\epsilon) = 0
\end{equation*}
and:
\begin{equation*}
    a(\epsilon) + b(\gamma - E) = 0
\end{equation*}

We solving this system of linear equations we find the possible MO energies to be:

\begin{equation*}
    E = E = \gamma \pm \epsilon
\end{equation*}

 and:
 
\begin{equation*}
    \Psi = \frac{1}{\sqrt{2}}(\psi \pm \Psi)
\end{equation*}

From this the simplicity of the Huckel method should be evident as much as its limitations. Using perturbation theory it can be applied to a slightly wider variety of problems but it is far from universal. Its power lies in its simplicity. 

% Huckel Method is used to predict energy levels of \pi electron orbitals in molecules. (This provides the theoretical background

% Part K of Lindsay's book has an example of the Huckel Theory using benzene.


\section{\label{sec:level1}Making a Rectifier}

A rectifier is an electronic component that allows current to flow in one direction, whilst effectively  blocking current in the other direction. Classically, this rectifying ability is achieved through an asymmetry in respect to the molecular organisation when a voltage bias is applied across the rectifier. A rectifier under forward bias allows the flow of charge, whereas under reverse bias, the device largely prohibits the flow of charges (Marcus Theory?).

A number of methods have been proposed and realised to make molecular diodes, with varying success and feasibility regarding out of lab applications. The rectifying ability in molecular diodes is generally achieved by an asymmetrically shaped molecule joining electrodes over a junction, different materials serving as the electrodes either side of the junction, or by the effective contact between the molecule and electrodes being made by differing linker molecules on either end of the molecule across the junction. One group of experimenters looked in to breaking symmetry across the junction by differing the effective exposed areas of the electrode\cite{diode}. In order to understand under what conditions this results in an effective diode, it is necessary to understand the underlying physics of the thing.

\subsection{\label{sec:level2}Electric Double Layer}

Any charged surface that is in contact with an environment that can provide neutralising charges will adsorb such charges onto its surface until the surface is neutralised\cite{book}, forming an Electric Double Layer (EDL) consisting of the surface charge, the layer of oppositely charged ions, and a diffuse layer where the concentration of the oppositely charged ions falls off to equilibrium\cite{stern}. The charges are dispersed in the medium according to the electrostatic attraction to the surface and thermal fluctuations, the innermost charges partially screen the electric field from the outermost ones, resulting in an exponentially decreasing electric field, with respect to the distance from the charged surface.

The distribution can be calculated by a Poisson-Boltzmann equation, treating the ions as point charges, this gives rise to a decay length (Debye). 

%Across conducting junction, use the length to find an area which is related to current. Take into account the ions are being neutralised, makng the debye length longer.

In a conducting solution with two oppositely externally charged electrodes producing a current, 

\subsection{\label{sec:level2}Stark shift}

The Stark Effect is observed when a charge distribution is placed in an external electric field. The electric field causes a separation of otherwise degenerate energy levels, which in turn shifts the HOMO and LUMO resonances of the molecule up and down respectively.

The effect can be considered in a simple case, a Hydrogen atom. The Hamiltonian of an electron in an electric field can be obtained by applying a perturbation, the Stark term (H_{Stark}), onto the Coulombic Hamiltonian,

\begin{equation*}
V(r) = - \dfrac{Zq^{2}}{4\pi\epsilon_{0}r}
\end{equation*}

which, changing to Gaussian units for convenience, in a hydrogen atom is 

\begin{equation*}
V(r) = - \dfrac{e^{2}}{r}
\end{equation*}

On the basis that the energy of the hydrogen atom is the sum of the kinetic ($\dfrac{mv^{2}}{2} $)and potential energies, we obtain the Hamiltonian for the electron 

\begin{equation*}
H = - \dfrac{h^{2}}{2\mu}\nabla^{2}- \dfrac{e^{2}}{r}
\end{equation*}

where \mu has been introduced as the electron's effective mass.

\subsection{\label{sec:level2}Scanning Tunnelling Microscopy-based Break Junction technique (STM-BJ) for a Resonant Tunnelling Diode}

\subsection{\label{sec:level2}Lorentzian Peak}

\subsection{\label{sec:level2}Landauer-like expression}

% As I understand it, the paper is saying that the symmetry is broken by the difference in Electric Double Layers that form on the substrate and the STM tip. In a polar solution, the ions form a double layer, which screens the bias as in the Debye model, effectively acting as a p-n junction.

Metal-Molecule-Metal

"We hypothesize that the double layer causes a bias polarity dependent shift of the molecular resonance energy within the junction, leading to rectification"

Electric Double Layer (Debye -> Marcus?. Differential capacitance, I'd like to do Debye-Huckel, but I don't think it's relevant.)

Landauer-like expression

DFT-Models if I can provide enough data for Bram

Conductance

(Sensitivity to junction structure)

Operating Voltage

% Coulomb Blocking
% (Finfet transistors)

\begin{thebibliography}{9}

\bibitem{MOSFET-tunnel}
M.-A. Jaud, S. Barraud and G. Le Carval (Mar. 7, 2004)
\textit{Impact of quantum mechanical tunneling on off-leakage current in double-gate MOSFET using a quantum drift-diffusion model}
2004 NSTI Nanotechnology Conference and Trade Show 2:17-20 [isbn:  0-9728422-8-4]

\bibitem{sub-10nm}
Jiaxin Zheng, Lu Wang, Ruge Quhe, Qihang Liu, Hong Li, Dapeng Yu, Wai-Ning Mei, Junjie Shi, Zhengxiang Gao and Jing Lu (Feb. 19, 2013)
\textit{Sub-10 nm Gate Length Graphene Transistors: Operating at Terahertz Frequencies with Current Saturation}
Nature Scientific Reports 3 (1314):1-9 [doi: 10.1038/srep01314]

\bibitem{nojunct}
Jean-Pierre Colinge, Chi-Woo Lee, Aryan Afzalian, Nima Dehdashti Akhavan, Ran Yan, Isabelle Ferain, Pedram Razavi, Brendan O’Neill, Alan Blake, Mary White, Anne-Marie Kelleher, Brendan McCarthy and Richard Murphy (Feb. 21, 2010)
\textit{Nanowire transistors without junctions}
Nature Nanotechnology 5: 225-229 [doi: 10.1038/nnano.2010.15]

\bibitem{singmol}
Sriharsha V. Aradhya and Latha Venkataraman (Jun. 5, 2013)
\textit{Single-molecule junctions beyond electronic transport}
Nature Nanotechnology 8: 399-410. [doi: 10.1038/nnano.2013.91]

\bibitem{NSgraphene}
Mason Inman (Apr. 17, 2008)
\textit{Atom-thick material runs rings around silicon}
New Scientist, 198 (2653).

\bibitem{5nm}
Hyunjin Lee, Lee-Eun Yu, Seong-Wan Ryu, Jin-Woo Han, Kanghoon Jeon, Dong-Yoon Jang, Kuk-Hwan Kim, Jiye Lee, Ju-Hyun Kim, Sang Cheol Jeon, Gi Seong Lee, Jae Sub Oh, Yun Chang Park, Woo Ho Bae, Hee Mok Lee, Jun Mo Yang*, Jung Jae Yoo, Sang Ik Kim and Yang-Kyu Choi (Jun. 13, 2006)
\textit{Sub-5nm All-Around Gate FinFET for Ultimate Scaling}
2006 Symposium on VLSI Technology, 2006. Digest of Technical Papers. 58-59 [doi: 10.1109/vlsit.2006.1705215]

\bibitem{grapheneqd}
L. A. Ponomarenko, F. Schedin, M. I. Katsnelson, R. Yang, E. W. Hill, K. S. Novoselov, A. K. Geim (Apr. 18 2008)
\textit{Chaotic Dirac Billiard in Graphene Quantum Dots}
Science 320 (5874) 356-358. [doi: 10.1126/science.1154663]

\bibitem{wired}
Robert F. Service (Dec. 21, 2001)
\textit{Molecules Get Wired}
Science 294 (5551), 2442-2443. [doi: 10.1126/science.294.5551.2442]

\bibitem{DNA}
Joseph C. Genereux and Jacqueline K. Barton (May, 2009)
\textit{DNA charges ahead}
Nature Chemistry 1:106-107

\bibitem{Marcus}
Andrea M. (2008)
\textit{Marcus theory for electron transfer a short introduction}
MPIP J. Club-Mainz:1–13.

\bibitem{e-transfer}
Han-sur-Lesse (2011)
\textit{Theory of electron transfer Electron transfer. Part II}
Winterschool Theor. Chem. Spectrosc.:12–16.

\bibitem{RRKM}
Di Giacomo F (2015)
\textit{A short account of RRKM theory of unimolecular reactions and of marcus theory of electron transfer in a historical perspective}
J. Chem. Educ. 92:476–481.

\bibitem{diode}
Brian Capozzi, Jianlong Xia, Olgun Adak, Emma J. Dell, Zhen-Fei Liu, Jeffrey C. Taylor, Jeffrey B. Neaton, Luis M. Campos and Latha Venkataraman (2015)
\textit{Single-molecule diodes with high rectification ratios through environmental control}
Nature Nanotechnology 10 522-527

\bibitem{stern}
Christian Schneider (2010)
\textit{The Surface Charge of Soft and Hard Sphere Colloidal Particles – Experimental Investigation and Comparison to Theory (Dissertation)}
Helmoltz Zentrum Berlin HZB – B 16

\end{thebibliography}
\end{document}
